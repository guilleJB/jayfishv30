<!DOCTYPE html>

<html>
<head>

       <script src="js/Chart.js"></script>
</head>

<!-- MY CODE -->

<?php
  include "include.php";
?>
<body>

<!-- ###################GRAPH START################################################ -->

<script>

    var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
    var lineChartData_month = {
      labels : [1,2,3
      ],
      datasets : [
       {
           label: "Water",
          fillColor : "rgba(100,100,205,0.2)",
          strokeColor : "rgba(151,187,205,1)",
          pointColor : "rgba(151,187,205,1)",
          pointStrokeColor : "#fff",
          pointHighlightFill : "#fff",
          pointHighlightStroke : "rgba(151,187,205,1)",
          data : [1,2,3

          ]
        },
        {
           label: "Air",
          fillColor : "rgba(200,200,200,0.3)",
          strokeColor : "rgba(220,220,220,1)",
          pointColor : "rgba(220,220,220,1)",
          pointStrokeColor : "#fff",
          pointHighlightFill : "#fff",
          pointHighlightStroke : "rgba(220,220,220,1)",
           data : [7,8,9

           ]
        }
      ]

    }

</script>

  <script>
  		window.onload = function(){
   			var ctxc = document.getElementById("canvas_month").getContext("2d");
    		window.myLine = new Chart(ctxc).Line(lineChartData_month, {responsive: false});
  }
  </script>




<div align="center">
    <table class="<?php print $tablebackground_nolines;?>" border="0" width="<?php print $tablewidth;?>">
    	<td style="padding-left:10px; padding-right:10px;">
       		<div align="center" class="plaindark customfont">TEMPLATE</div></td><tr>
     	<td>
	        	<div style="width:100%;"><div>
	                 <canvas id="canvas_month" height="250" width="900"></canvas>
            </div> 
            </td>
          </table> 
        <!-- </div> -->
   <!-- </div> -->
</div>
<!-- ###################GRAPH END################################################ -->


</body>
</html>









